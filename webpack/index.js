require('bootstrap/dist/css/bootstrap.min.css');
require('../public/css/default.css');

// import modules without parseing with script-loader
// !! - override all loaders
require('!!script!angular2/bundles/angular2-polyfills.min.js');
require('!!script!systemjs/dist/system.src.js');
require('!!script!rxjs/bundles/Rx.js');
require('!!script!angular2/bundles/angular2.dev.js');