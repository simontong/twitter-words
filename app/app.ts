/// <reference path='../typings/tsd.d.ts' />

/**
 * import deps
 */
import express = require('express');
import logger = require('morgan');
import bodyParser = require('body-parser');
import methodOverride = require('method-override');
import mongoose = require('mongoose');
import path = require('path');

/**
 * import controllers
 */
import MainController from './controllers/main';

/**
 * Config app
 */
let app = express();
app.set('env', 'dev');
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(logger(app.get('env')));
app.use(bodyParser.json());
app.use(methodOverride());

// public path to serve
app.use(express.static(path.join(__dirname, '..', 'public')));

// node modules to use
app.use('/angular2', express.static(path.join(__dirname, '..', 'node_modules', 'angular2')));

/**
 * config routes
 */
app.get('/', MainController.getIndex);
app.get('/templates/:template', MainController.getTemplate);

/**
 * start app
 */
app.listen(app.get('port'), () => {
    console.log(`App listening on port ${app.get('port')}`);
});