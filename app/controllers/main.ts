class MainController {
    static getIndex(req, res) {
        res.render('index');
    }

    static getTemplate(req, res) {
        res.render('/templates/' + req.params.template);
    }
}

export default MainController;