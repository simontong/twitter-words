import {Component, View} from 'angular2/core';
import WordsService from './words.service';

class Word {
    public word: String;
}

@Component({
    selector: 'words-app',
    template: `
        <div class="container">
            <ul>
                <li *ngFor="#word of words">{{word.word}}</li>
            </ul>
        </div>
    `
})
export default class WordsComponent {
    public words: Word[] = [];

    constructor(public wordsService: WordsService) {
        var word;

        word = new Word;
        word.word = 'simon';
        this.words.push(word);

        word = new Word;
        word.word = 'sarah';
        this.words.push(word);

        word = new Word;
        word.word = 'hello';
        this.words.push(word);

        word = new Word;
        word.word = 'like';
        this.words.push(word);
    }
}