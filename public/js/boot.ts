import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import WordsComponent from './words.component';

bootstrap(WordsComponent, [HTTP_PROVIDERS]);